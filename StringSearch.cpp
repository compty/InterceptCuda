#include <string>
#include "StringSearch.h"
#include <dlfcn.h>
#include <cstdint>

//std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find(char const*, unsigned long) const
extern "C" size_t _ZNKSs4findEPKcm(void* thisp, const char * input, std::size_t pos) {

    typedef size_t (*original_find)(void* thisp, const char*, size_t);

    std::basic_string<char>* basic_str = reinterpret_cast<std::basic_string<char> *>(thisp);

    //return ((original_find)dlsym(RTLD_NEXT, "_ZNKSs4findEPKcm"))(thisp, input, pos);

    return find(basic_str->data(), input, pos);
}


std::size_t find(const char* original_string, const char* search_string, std::size_t pos) {

    if(*original_string == '\0' && *search_string == '\0') {
        return 0;
    }

    char *current_pos = const_cast<char *>(original_string + pos);
    char *start_pos = current_pos;
    char *search_pos = current_pos;

    while (*current_pos != '\0') {

        search_pos = current_pos;
        uint64_t search_counter = 0;


        while (*(search_pos + search_counter) == *(search_string + search_counter) && *(search_string + search_counter) != '\0') {
            search_counter++;
        }

        if (*(search_string + search_counter) == '\0') {
            return (size_t) (search_pos - (start_pos - pos));
        }

        current_pos++;
    }

    return (size_t) -1;
}

