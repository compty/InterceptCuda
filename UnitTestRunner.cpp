#include <iostream>
#include <memory>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/TextOutputter.h>

#include "ExecutionTimeListener.h"
#include "MD5SumTest.h"
#include "StringSearchTest.h"

int main(int argc, char *argv[]) {

    CppUnit::TestResult controller;

    CppUnit::TestResultCollector result; 
    ExecutionTimeListener timer;
    controller.addListener ( &result ); 
    controller.addListener( &timer );
    auto runner = std::make_shared<CppUnit::TextUi::TestRunner>(); 
 
    std::ofstream resultLocation("testResults.xml");

    CppUnit::XmlOutputter xmlOutputter(&result, resultLocation);

    CppUnit::TextOutputter consoleOutputter(&result,std::cout); 

    runner->addTest(MD5Test::suite());
    runner->addTest(StringSearchTest::suite());

    runner->run(controller); 

    xmlOutputter.write();

    consoleOutputter.write();

    return result.wasSuccessful() ? 0 : 1;
}
