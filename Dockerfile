FROM nvidia/cuda:9.2-devel-centos7

# enable the epel repo
RUN yum -y install epel-release
# install required build utils
RUN yum -y install cmake3 automake make gcc-c++ cppunit-devel rpm-build
RUN useradd jenkins
