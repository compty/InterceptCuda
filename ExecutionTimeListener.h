#pragma once

#include <chrono>

#include <cppunit/Test.h>
#include <cppunit/TestListener.h>

class ExecutionTimeListener : public CppUnit::TestListener {

public:

    ExecutionTimeListener();
    ~ExecutionTimeListener();

    void startTest(CppUnit::Test *test);
    void endTest(CppUnit::Test * test);

private:

    std::chrono::time_point<std::chrono::system_clock> start_time;
};
