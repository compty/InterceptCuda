Name: InterceptCuda		
Version: 1.0.0
Release: 1%{?dist}
Summary: Library for intercepting certain calls to functions. For example intercepting calls to md5::md5sum. Requires LD_PRELOAD to be updated with the path to /usr/local/lib/libintercept-cuda.so	

Group: NA	
License: NA	
URL: NA	
Source0: %{name}-%{version}.tar.gz

BuildRequires: cppunit-devel

%description
Library for intercepting certain calls to functions. For example intercepting calls to md5::md5sum. Requires LD_PRELOAD to be updated with the path to /usr/local/lib/libInterceptCuda.so

%prep
%setup

%build
mkdir build
cd build
cmake3 ../
make %{?_smp_mflags}

%install
cd build
make PREFIX=/usr DESTDIR=%{?buildroot} install

%clean
rm -rf %{buildroot}

%files
/usr/local/lib/*.so
