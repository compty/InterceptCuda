#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "MD5SumTest.h"
#include "MD5Sum.h"

// Cuda lazy initializes it's context. So just a noddy test to force a cuda call to init the context
void MD5Test::InitialiserForCuda() {
    md5::md5sum("");
    CPPUNIT_ASSERT(true);
}

void MD5Test::MD5TestEmptyString() {
    CPPUNIT_ASSERT_EQUAL(std::string("d41d8cd98f00b204e9800998ecf8427e"), md5::md5sum(""));
}

void MD5Test::MD5TestSimple() {
    CPPUNIT_ASSERT_EQUAL(std::string("098f6bcd4621d373cade4e832627b4f6"), md5::md5sum("test"));
}

void MD5Test::MD5TestSimpleBelowBoundary() {
    CPPUNIT_ASSERT_EQUAL(std::string("55cd2251fa149100775adb7356828c9f"), md5::md5sum("THISISATESTSTRING"));
}

void MD5Test::MD5TestBoundary() {
    CPPUNIT_ASSERT_EQUAL(std::string("93d268e9bef6608ff1a6a96adbeee106"), md5::md5sum("This string is precisely 56 characters long for a reason"));
}

void MD5Test::MD5TestLargerThanBoundary() {
    CPPUNIT_ASSERT_EQUAL(std::string("2454a169f5a600e27c9930872bd1ed3b"), md5::md5sum("This string is precisely 57 characters long for a reason!"));
}

void MD5Test::MD5TestAFairlySizableInput() {
    CPPUNIT_ASSERT_EQUAL(std::string("be7378b801dfbe3c71a48ea3cd917d73"), md5::md5sum("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan dui maximus venenatis cursus. Nam consectetur nisl urna, ut ultricies velit luctus nec. Pellentesque nisl justo, scelerisque at aliquet vel, pellentesque a arcu. Aliquam gravida lorem non felis aliquet convallis. Morbi tincidunt semper congue. Maecenas eget justo arcu. Maecenas dignissim dapibus gravida. Proin sed vulputate purus. Etiam nec nisi sit amet nisi pulvinar sodales sit amet nec dolor. Sed aliquet turpis erat, vitae interdum mi posuere quis. Suspendisse potenti. Nulla neque purus, ornare in bibendum non, consectetur ac neque. Curabitur tortor odio, vulputate vitae porta nec, mattis vel ipsum. Etiam nec eros mi. Nunc non dolor ante."));
}
