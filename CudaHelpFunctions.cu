#include <iostream>

#define CudaErrorCheck(ans) { GPUErrorCheck((ans), __FILE__, __LINE__); }
void GPUErrorCheck(cudaError_t errCode, const char *file, int line)
{
   if (errCode != cudaSuccess)
   {
      fprintf(stderr,"GPUErrorCheck: %s %s %d\n", cudaGetErrorString(errCode), file, line);
      if (abort) {
          exit(errCode);
       }
   }
}

void GPUPrintDeviceProperties() {

    int dev_count;
    cudaGetDeviceCount(&dev_count);

    cudaDeviceProp dev_prop;
    for(uint64_t i = 0; i < dev_count; ++i) {
        cudaGetDeviceProperties(&dev_prop, i);
        std::cout << "Multiprocessor count: " << dev_prop.multiProcessorCount << std::endl;
        std::cout << "Clock rate:" << dev_prop.clockRate << std::endl;
        std::cout << "Max threads per block: " << dev_prop.maxThreadsPerBlock << std::endl;
        std::cout << "Max threads per Dim (x): " << dev_prop.maxThreadsDim[0] << std::endl;
        std::cout << "Max threads per Dim (y): " << dev_prop.maxThreadsDim[1] << std::endl;
        std::cout << "Max grid size(x): " << dev_prop.maxGridSize[0] << std::endl;
        std::cout << "Max grid size(y): " << dev_prop.maxGridSize[1] << std::endl;
        std::cout << "Max grid size(z): " << dev_prop.maxGridSize[2] << std::endl;
    }
}
