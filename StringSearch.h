#pragma once
#include <cstdio>
std::size_t find(const char* original_string, const char* search_string, std::size_t pos = 0);
