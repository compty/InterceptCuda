#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "StringSearchTest.h"
#include "StringSearch.h"

void StringSearchTest::StringSearchEmptyString() {

    std::string test_string("A real input string");

    // Zero because empty string is a substring of all strings!
    CPPUNIT_ASSERT_EQUAL((unsigned long)0, test_string.find(""));
}

void StringSearchTest::StringSearchWithMatchAtStart() {
    std::string test_string("A real input string");

    CPPUNIT_ASSERT_EQUAL((unsigned long)0, test_string.find("A real"));
}

void StringSearchTest::StringSearchWithMatchInMiddle() {
    std::string test_string("A real input string");

    CPPUNIT_ASSERT_EQUAL((unsigned long)2, test_string.find("real"));
}

void StringSearchTest::StringSearchWithMatchAtEnd() {
    std::string test_string("A real input string");
    CPPUNIT_ASSERT_EQUAL((unsigned long)13, test_string.find("string"));
}

void StringSearchTest::StringSearchWithSingleCharacterMatchAtStart() {
    std::string test_string("A real input string");

    CPPUNIT_ASSERT_EQUAL((unsigned long)0, test_string.find("A"));
}

void StringSearchTest::StringSearchWithSingleCharacterMatchInMiddle() {
    std::string test_string("A real input string");
    CPPUNIT_ASSERT_EQUAL((unsigned long)7, test_string.find("i"));
}

void StringSearchTest::StringSearchWithSingleCharacterMatchAtEnd() {
    std::string test_string("A real input string");

    CPPUNIT_ASSERT_EQUAL((unsigned long)18, test_string.find("g"));
}

void StringSearchTest::StringSearchWithoutMatch() {
    std::string test_string;
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("Different"));
}

void StringSearchTest::StringSearchWithSearchStringLargerThanInputString() {
    std::string test_string("Some small string");
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("A really really really really large string"));
}

void StringSearchTest::StringSearchWithSearchStringExactMatch() {
    std::string test_string("An exact match");
    CPPUNIT_ASSERT_EQUAL((unsigned long)0, test_string.find("An exact match"));
}

void StringSearchTest::StringSearchWithWithMultipleMatches() {
    std::string test_string("A string with multiple string matches");
    CPPUNIT_ASSERT_EQUAL((unsigned long)2, test_string.find("string"));
}

void StringSearchTest::StringSearchWithEmptyBaseString() {
    std::string test_string("");
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("string"));
}

void StringSearchTest::StringSearchMatchWithStartPos() {
    std::string test_string("A string to test with pos");
    CPPUNIT_ASSERT_EQUAL((unsigned long)9, test_string.find("to", 5));
}

void StringSearchTest::StringSearchWithoutMatchWithStartPos() {
    std::string test_string("A string to test with pos");
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("Not a match", 4));
}

void StringSearchTest::StringSearchWithMatchingStringBeforeStartPos() {
    std::string test_string("A string to test with pos");
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("string", 10));
}

void StringSearchTest::StringSearchWithBaseAndSearchStringEmpty() {
    std::string test_string("");
    CPPUNIT_ASSERT_EQUAL((unsigned long)0, test_string.find(""));
}

void StringSearchTest::StringSearchWithPosOffEndOfString() {
    std::string test_string("Sample String");
    CPPUNIT_ASSERT_EQUAL((unsigned long)-1, test_string.find("Sample", 10000));
}
