#include <iostream>
#include "MD5Cuda.h"

int main() {
    int deviceCount;
    cudaError_t err = cudaGetDeviceCount(&deviceCount);
    if(err == cudaSuccess)
        std::cout << deviceCount << std::endl;
    else {
        std::cout << "ERROR" << std::endl;
        std::cout << err << std::endl;
    }

    std::string teststring("test");
    std::cout << md5::md5sum(teststring) << std::endl;
}
