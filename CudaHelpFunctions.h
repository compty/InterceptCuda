#pragma once

#define CudaErrorCheck(ans) { GPUErrorCheck((ans), __FILE__, __LINE__); }
void GPUErrorCheck(cudaError_t errCode, const char *file, int line);

void GPUPrintDeviceProperties();
