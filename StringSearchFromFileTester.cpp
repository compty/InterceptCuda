#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <chrono>

int main(int argc, char** argv) {
    std::cout << "Opening: " << argv[1] << std::endl;
    std::ifstream inputFile(argv[1]);
    std::string testStr((std::istreambuf_iterator<char>(inputFile)),
                     std::istreambuf_iterator<char>());
    testStr.find("not");
    auto startTime = std::chrono::system_clock::now();
    testStr.find("test");
    auto endTime = std::chrono::system_clock::now();

    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);
    std::cout << "Time to execute: " << microseconds.count() << std::endl;
}
