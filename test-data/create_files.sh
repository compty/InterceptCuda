#!/bin/bash

#Generates 19 files ranging from 1KB to 1GB

bytes_calc=512
for i in $(seq 1 21);
do
    bytes_calc="$(($bytes_calc*2))"
    perl -se 'print "1" x "$bytes"' -- -bytes=${bytes_calc} > file$i
done
