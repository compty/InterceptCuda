# Intercept Cuda

Intercept Cuda is a software project aimed at accelerating certain functions by intercepting the calls to the original intended function and offloading the processing to an Nvidia GPU.

## Getting Started

The following instructions will detail how to build and install the code and how to get it working on your machine.

### Prerequisites

Hardware specifications this project was developed with

```
Nvidia 780ti GPU Clock 1006Mhz Memory 3500MHz
Intel I7 4770k 3.5Ghz
```

OS, Drivers & build tools

```
CentOS Linux release 7.5.1804
Nvidia linux driver 390.67
    - https://www.nvidia.com/object/unix.html
Cuda 9.2
    - https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=CentOS&target_version=7
gcc-4.8.5
cmake3
automake

```

Optionally to build the project within a docker container & using jenkins
```
Docker
Nvidia docker
    - https://github.com/NVIDIA/nvidia-docker
    - Follow install for Centos 7 (docker)

Jenkins
```

### Installing

The project can be built by doing the following
```
mkdir build
cd build
cmake ..
make -j4
```

To run the tests
```
make test
```

To install to /usr/local/lib
```
make install
```

## Running the tests

Tests for the software can be run by doing the following
```
make test
```

## Deployment

To install onto a real system, build the rpm via jenkins using the jenkinsfile provided and the docker image

To build the docker image run
```
docker build . -t intercept_cuda_build
```

Alternatively you can use the docker image already built and available from the docker registry for the InterceptCuda project. First login to the gitlab docker registry:
```
docker login registry.gitlab.com
```

Then download the docker image:
```
docker pull registry.gitlab.com/compty/interceptcuda/intercept_cuda_build
```

The jenkinsfile builds the rpm using the docker image created above and archives the rpm so it can be retrieved from the jenkins job.

Once you have built the rpm, copy onto the desired system and run

```
rpm --install /tmp/InterceptCuda-1.0.0-1.el7.x86_64.rpm
```

And to run your program with the intercepted function calls offloaded to gpu simply do the following
```
LD_PRELOAD=/usr/local/lib/libInterceptCuda.so ./YOUR_PROGRAM_HERE
```

If you only want to run the cuda version of the function every time set the environment flag
```
CUDA_ONLY=true
```

And to run your program with the intercepted function calls with the non gpu version, do the following
```
LD_PRELOAD=/usr/local/lib/libNonCudaIntercept.so ./YOUR_PROGRAM_HERE
```

## Authors

* **David Compton** - *Initial work* - [InterceptCuda](https://gitlab.com/compty/InterceptCuda)
