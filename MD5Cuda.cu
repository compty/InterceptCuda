#include "MD5Cuda.h"
#include "CudaHelpFunctions.h"
#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cstring>

__device__ void md5::processBlock(const char *block, uint32_t* A, uint32_t* B, uint32_t* C, uint32_t* D) {

    uint32_t X[16] = {'\0'};
    for (uint8_t i = 0; i < 16; i++) {
        std::memcpy(X + i, block + (4 * i), 4);
    }

    uint32_t A_ORIG = *A;
    uint32_t B_ORIG = *B;
    uint32_t C_ORIG = *C;
    uint32_t D_ORIG = *D;

    /* Round 1 */
    FF (*A, *B, *C, *D, X[0], shifts[0], k[0]); /* 1 */
    FF (*D, *A, *B, *C, X[1], shifts[1], k[1]); /* 2 */
    FF (*C, *D, *A, *B, X[2], shifts[2], k[2]); /* 3 */
    FF (*B, *C, *D, *A, X[3], shifts[3], k[3]); /* 4 */
    FF (*A, *B, *C, *D, X[4], shifts[0], k[4]); /* 5 */
    FF (*D, *A, *B, *C, X[5], shifts[1], k[5]); /* 6 */
    FF (*C, *D, *A, *B, X[6], shifts[2], k[6]); /* 7 */
    FF (*B, *C, *D, *A, X[7], shifts[3], k[7]); /* 8 */
    FF (*A, *B, *C, *D, X[8], shifts[0], k[8]); /* 9 */
    FF (*D, *A, *B, *C, X[9], shifts[1], k[9]); /* 10 */
    FF (*C, *D, *A, *B, X[10], shifts[2], k[10]); /* 11 */
    FF (*B, *C, *D, *A, X[11], shifts[3], k[11]); /* 12 */
    FF (*A, *B, *C, *D, X[12], shifts[0], k[12]); /* 13 */
    FF (*D, *A, *B, *C, X[13], shifts[1], k[13]); /* 14 */
    FF (*C, *D, *A, *B, X[14], shifts[2], k[14]); /* 15 */
    FF (*B, *C, *D, *A, X[15], shifts[3], k[15]); /* 16 */

    /* Roun*D 2 */
    GG (*A, *B, *C, *D, X[1], shifts[4], k[16]); /* 17 */
    GG (*D, *A, *B, *C, X[6], shifts[5], k[17]); /* 18 */
    GG (*C, *D, *A, *B, X[11], shifts[6], k[18]); /* 19 */
    GG (*B, *C, *D, *A, X[0], shifts[7], k[19]); /* 20 */
    GG (*A, *B, *C, *D, X[5], shifts[4], k[20]); /* 21 */
    GG (*D, *A, *B, *C, X[10], shifts[5], k[21]); /* 22 */
    GG (*C, *D, *A, *B, X[15], shifts[6], k[22]); /* 23 */
    GG (*B, *C, *D, *A, X[4], shifts[7], k[23]); /* 24 */
    GG (*A, *B, *C, *D, X[9], shifts[4], k[24]); /* 25 */
    GG (*D, *A, *B, *C, X[14], shifts[5], k[25]); /* 26 */
    GG (*C, *D, *A, *B, X[3], shifts[6], k[26]); /* 27 */
    GG (*B, *C, *D, *A, X[8], shifts[7], k[27]); /* 28 */
    GG (*A, *B, *C, *D, X[13], shifts[4], k[28]); /* 29 */
    GG (*D, *A, *B, *C, X[2], shifts[5], k[29]); /* 30 */
    GG (*C, *D, *A, *B, X[7], shifts[6], k[30]); /* 31 */
    GG (*B, *C, *D, *A, X[12], shifts[7], k[31]); /* 32 */

    /* Round 3 */
    HH (*A, *B, *C, *D, X[5], shifts[8], k[32]); /* 33 */
    HH (*D, *A, *B, *C, X[8], shifts[9], k[33]); /* 34 */
    HH (*C, *D, *A, *B, X[11], shifts[10], k[34]); /* 35 */
    HH (*B, *C, *D, *A, X[14], shifts[11], k[35]); /* 36 */
    HH (*A, *B, *C, *D, X[1], shifts[8], k[36]); /* 37 */
    HH (*D, *A, *B, *C, X[4], shifts[9], k[37]); /* 38 */
    HH (*C, *D, *A, *B, X[7], shifts[10], k[38]); /* 39 */
    HH (*B, *C, *D, *A, X[10], shifts[11], k[39]); /* 40 */
    HH (*A, *B, *C, *D, X[13], shifts[8], k[40]); /* 41 */
    HH (*D, *A, *B, *C, X[0], shifts[9], k[41]); /* 42 */
    HH (*C, *D, *A, *B, X[3], shifts[10], k[42]); /* 43 */
    HH (*B, *C, *D, *A, X[6], shifts[11], k[43]); /* 44 */
    HH (*A, *B, *C, *D, X[9], shifts[8], k[44]); /* 45 */
    HH (*D, *A, *B, *C, X[12], shifts[9], k[45]); /* 46 */
    HH (*C, *D, *A, *B, X[15], shifts[10], k[46]); /* 47 */
    HH (*B, *C, *D, *A, X[2], shifts[11], k[47]); /* 48 */


    /* Roun*D 4 */
    II (*A, *B, *C, *D, X[0], shifts[12], k[48]); /* 49 */
    II (*D, *A, *B, *C, X[7], shifts[13], k[49]); /* 50 */
    II (*C, *D, *A, *B, X[14], shifts[14], k[50]); /* 51 */
    II (*B, *C, *D, *A, X[5], shifts[15], k[51]); /* 52 */
    II (*A, *B, *C, *D, X[12], shifts[12], k[52]); /* 53 */
    II (*D, *A, *B, *C, X[3], shifts[13], k[53]); /* 54 */
    II (*C, *D, *A, *B, X[10], shifts[14], k[54]); /* 55 */
    II (*B, *C, *D, *A, X[1], shifts[15], k[55]); /* 56 */
    II (*A, *B, *C, *D, X[8], shifts[12], k[56]); /* 57 */
    II (*D, *A, *B, *C, X[15], shifts[13], k[57]); /* 58 */
    II (*C, *D, *A, *B, X[6], shifts[14], k[58]); /* 59 */
    II (*B, *C, *D, *A, X[13], shifts[15], k[59]); /* 60 */
    II (*A, *B, *C, *D, X[4], shifts[12], k[60]); /* 61 */
    II (*D, *A, *B, *C, X[11], shifts[13], k[61]); /* 62 */
    II (*C, *D, *A, *B, X[2], shifts[14], k[62]); /* 63 */
    II (*B, *C, *D, *A, X[9], shifts[15], k[63]); /* 64 */

    *A += A_ORIG;
    *B += B_ORIG;
    *C += C_ORIG;
    *D += D_ORIG;
}

__device__ uint8_t md5::leftRotate(const uint32_t x, const uint8_t c) {
    return (x << c) | (x >> (32 - c));
}

void md5::uint32ToCharArray(char *buf, uint32_t val) {

    uint8_t *p = (uint8_t*) &val;
    sprintf(buf, "%2.2x", p[0]);
    sprintf(buf + 2, "%2.2x", p[1]);
    sprintf(buf + 4, "%2.2x", p[2]);
    sprintf(buf + 6, "%2.2x", p[3]);
}

__device__ uint32_t md5::charArrayToUint32(char *buf) {

    uint32_t retval = (uint32_t) buf[0] << 24 |
                      (uint32_t) buf[1] << 16 |
                      (uint32_t) buf[2] << 8 |
                      (uint32_t) buf[3];

    return retval;
}

__global__ void md5::md5cuda(char* input, std::size_t length, uint32_t* deviceResult) {
    
    uint32_t A = md5::A_INIT;
    uint32_t B = md5::B_INIT;
    uint32_t C = md5::C_INIT;
    uint32_t D = md5::D_INIT;

    uint64_t inputLengthMod = (length * 8) % 512;

    uint64_t paddingRequired = (length * 8) % 512;

    while (((inputLengthMod+ paddingRequired) % (512) != 448)) {
        ++paddingRequired;
    }

    if (length * 8 < 448) {
        paddingRequired = 448 - (length * 8);
    }

    uint64_t inputPaddedSize = (((length * 8) + paddingRequired)/8);
    char* inputPadded = new char[inputPaddedSize + 4];

    // bit '1' - with 7 0s
    inputPadded[length] = (char)128;

    uint32_t lengthMod64 = (length * 8);

    // copy the input into the inputPadded buffer
    for(int i = 0; i < length; ++i) {
        inputPadded[i] = input[i];
    }

    // copy the padding needed into the inputPadded buffer
    for(int i = length + 1; i < (paddingRequired/8) + length; ++i) {
        inputPadded[i] = '\0';
    }

    inputPadded[length + (paddingRequired/8)] = lengthMod64 & 0xFF;
    inputPadded[length + (paddingRequired/8) + 1] = (lengthMod64 & 0xFF00) >> 8;
    inputPadded[length + (paddingRequired/8) + 2] = (lengthMod64 & 0xFF0000) >> 16;
    inputPadded[length + (paddingRequired/8) + 3] = (lengthMod64 & 0xFF000000) >> 24;

    inputPadded[length + (paddingRequired/8) + 4] = 0;
    inputPadded[length + (paddingRequired/8) + 5] = 0;
    inputPadded[length + (paddingRequired/8) + 6] = 0;
    inputPadded[length + (paddingRequired/8) + 7] = 0;

    for (uint16_t blockCounter = 0; blockCounter < inputPaddedSize; blockCounter += 64) {
        processBlock(inputPadded + blockCounter, &A, &B, &C, &D);
    }

    deviceResult[0] = A;
    deviceResult[1] = B;
    deviceResult[2] = C;
    deviceResult[3] = D;

    return;   
}

std::string md5::md5sum(const std::string & input) {

    dim3 dimGrid(1);
    dim3 dimBlock(1);

    char digest[33] = {'\0'};;
    
    char *inputString;
    cudaMalloc((void**)&inputString, input.length() * sizeof(char));
    CudaErrorCheck(cudaMemcpy(inputString, input.c_str(), input.length() * sizeof(char), cudaMemcpyHostToDevice));

    uint32_t *deviceResult;
    uint32_t *hostResult = (uint32_t*) malloc(4 * sizeof(uint32_t));;
    cudaError_t err = cudaMalloc((void**)&deviceResult, 4 * sizeof(uint32_t));
    md5cuda<<<dimGrid, dimBlock>>>(inputString, input.length(), deviceResult);    
    CudaErrorCheck(cudaMemcpy(hostResult, deviceResult, 4 * sizeof(uint32_t), cudaMemcpyDeviceToHost));

    cudaFree(deviceResult);

    uint32ToCharArray(digest, hostResult[0]);
    uint32ToCharArray(digest + 8, hostResult[1]);
    uint32ToCharArray(digest + 16, hostResult[2]);
    uint32ToCharArray(digest + 24, hostResult[3]);

    return std::string(digest);
}
