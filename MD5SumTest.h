#pragma once

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class MD5Test : public CppUnit::TestFixture {

    CPPUNIT_TEST_SUITE(MD5Test);
        CPPUNIT_TEST(InitialiserForCuda);
        CPPUNIT_TEST(MD5TestEmptyString);
        CPPUNIT_TEST(MD5TestSimple);
        CPPUNIT_TEST(MD5TestSimpleBelowBoundary);
        CPPUNIT_TEST(MD5TestBoundary);
        CPPUNIT_TEST(MD5TestLargerThanBoundary);
        CPPUNIT_TEST(MD5TestAFairlySizableInput);

    CPPUNIT_TEST_SUITE_END();

protected:
    void InitialiserForCuda();

    void MD5TestEmptyString();

    void MD5TestSimple();

    void MD5TestSimpleBelowBoundary();

    void MD5TestBoundary();

    void MD5TestLargerThanBoundary();

    void MD5TestAFairlySizableInput();

};
