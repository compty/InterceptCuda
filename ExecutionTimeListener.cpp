#include <iostream>
#include "ExecutionTimeListener.h"

ExecutionTimeListener::ExecutionTimeListener() : start_time(){
}

ExecutionTimeListener::~ExecutionTimeListener() {

}

void ExecutionTimeListener::startTest(CppUnit::Test* test) {
    start_time = std::chrono::system_clock::now();
}

void ExecutionTimeListener::endTest(CppUnit::Test* test) {
    auto end_time = std::chrono::system_clock::now();
    std::cout << test->getName() << " took " << std::chrono::duration_cast<std::chrono::microseconds>(end_time-start_time).count() << "ms to execute" << std::endl;
}
