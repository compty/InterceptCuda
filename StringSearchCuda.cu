#include <iostream>
#include <memory>
#include <string>
#include <dlfcn.h>
#include "StringSearchCuda.h"
#include "CudaHelpFunctions.h"

// std::basic_string<char>::find
extern "C" size_t _ZNKSs4findEPKcm(void* thisp, const char * input, std::size_t pos) {

    typedef size_t (*original_find)(void* thisp, const char*, size_t);

    char* CUDA_ONLY;
    CUDA_ONLY = getenv("CUDA_ONLY");

    std::basic_string<char>* basic_str = reinterpret_cast<std::basic_string<char> *>(thisp);

    if(*input == '\0') {
        return 0;
    } else if(basic_str->length() < pos) {
        return (size_t)-1;
    }

    // call our cuda version if string is greater than 512
    if(basic_str->length() > 512 || CUDA_ONLY) {

        uint64_t length_minus_pos = basic_str->length() - pos;
        uint64_t thread_count;

        // We will always need at least one block
        uint64_t blocks_required = 1;

        /*
        Using a 780ti for development of this, hardware stats dictate the following
        Multiprocessor count: 15
        Clock rate: 1071500
        Max threads per block: 1024
        Max threads per Dim (x): 1024
        Max threads per Dim (y): 1024
        Max grid size(x): 2147483647
        Max grid size(y): 65535
        Max grid size(z): 65535
        */

        if(length_minus_pos < 256) {
            thread_count = length_minus_pos;
        } else {
            thread_count = 256;
            blocks_required = 1;
            if(basic_str->length()/256 > 1) {
                blocks_required = (basic_str->length()/256) + 1;
            }
        }

        dim3 threadsPerBlock(thread_count, 1);
        dim3 numBlocks(blocks_required, 1);
 
        char *original_string;
        cudaMalloc((void**)&original_string, (basic_str->length() - pos + 1) * sizeof(char));
        CudaErrorCheck(cudaMemcpy(original_string, basic_str->c_str() + pos, (basic_str->length() - pos) * sizeof(char), cudaMemcpyHostToDevice));

        char *search_string;
        cudaMalloc((void**)&search_string, (strlen(input) + 1) * sizeof(char));
        CudaErrorCheck(cudaMemcpy(search_string, input, strlen(input) * sizeof(char), cudaMemcpyHostToDevice));

        size_t* deviceResult;
        std::shared_ptr<size_t> hostResult = std::make_shared<size_t>(-1);
        cudaError_t err = cudaMalloc((size_t**)&deviceResult, sizeof(size_t)); 
        CudaErrorCheck(cudaMemcpy(deviceResult, hostResult.get(), sizeof(size_t), cudaMemcpyHostToDevice));

        CudaStringSearch<<<numBlocks,threadsPerBlock>>>(original_string, search_string, deviceResult);

        cudaMemcpy(hostResult.get(), deviceResult, sizeof(size_t), cudaMemcpyDeviceToHost);

        cudaFree(deviceResult);

        size_t returnResult = *hostResult;
        if(returnResult != (size_t)-1) {
            returnResult += pos;
        }

        // obviously we didn't find a match
        return returnResult;
    } else {
        // Call through to original find
        return ((original_find)dlsym(RTLD_NEXT, "_ZNKSs4findEPKcm"))(thisp, input, pos);
    }

}

__global__ void CudaStringSearch(const char* original_string, const char* search_string, size_t* device_result) {

    uint i = (blockIdx.x * blockDim.x) + threadIdx.x;

    if(*original_string == '\0' && *search_string == '\0') {
        return;
    }

    char *current_pos = const_cast<char *>(original_string) + i;
    char *search_pos = current_pos;

        search_pos = current_pos;
        uint64_t search_counter = 0;

        while ((*(search_pos + search_counter) == *(search_string + search_counter)) && *(search_string + search_counter) != '\0') {
            search_counter++;
        }
        // we have matched the whole search string
        if (*(search_string + search_counter) == '\0') {
            atomicMin((unsigned long long*)device_result, (unsigned long long)i);
            return;
        }

        current_pos++;

    return;
}
