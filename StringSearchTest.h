#pragma once

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class StringSearchTest : public CppUnit::TestFixture {

CPPUNIT_TEST_SUITE(StringSearchTest);

        CPPUNIT_TEST(StringSearchEmptyString);
        CPPUNIT_TEST(StringSearchWithMatchAtStart);
        CPPUNIT_TEST(StringSearchWithMatchInMiddle);
        CPPUNIT_TEST(StringSearchWithMatchAtEnd);
        CPPUNIT_TEST(StringSearchWithSingleCharacterMatchAtStart);
        CPPUNIT_TEST(StringSearchWithSingleCharacterMatchInMiddle);
        CPPUNIT_TEST(StringSearchWithSingleCharacterMatchAtEnd);
        CPPUNIT_TEST(StringSearchWithoutMatch);
        CPPUNIT_TEST(StringSearchWithSearchStringLargerThanInputString);
        CPPUNIT_TEST(StringSearchWithSearchStringExactMatch);
        CPPUNIT_TEST(StringSearchWithWithMultipleMatches);
        CPPUNIT_TEST(StringSearchWithEmptyBaseString);
        CPPUNIT_TEST(StringSearchMatchWithStartPos);
        CPPUNIT_TEST(StringSearchWithoutMatchWithStartPos);
        CPPUNIT_TEST(StringSearchWithMatchingStringBeforeStartPos);
        CPPUNIT_TEST(StringSearchWithBaseAndSearchStringEmpty);
        CPPUNIT_TEST(StringSearchWithPosOffEndOfString);

    CPPUNIT_TEST_SUITE_END();

protected:
    void StringSearchEmptyString();

    void StringSearchWithMatchAtStart();

    void StringSearchWithMatchInMiddle();

    void StringSearchWithMatchAtEnd();

    void StringSearchWithSingleCharacterMatchAtStart();

    void StringSearchWithSingleCharacterMatchInMiddle();

    void StringSearchWithSingleCharacterMatchAtEnd();

    void StringSearchWithoutMatch();

    void StringSearchWithSearchStringLargerThanInputString();

    void StringSearchWithSearchStringExactMatch();

    void StringSearchWithWithMultipleMatches();

    void StringSearchWithEmptyBaseString();

    void StringSearchMatchWithStartPos();

    void StringSearchWithoutMatchWithStartPos();

    void StringSearchWithMatchingStringBeforeStartPos();

    void StringSearchWithBaseAndSearchStringEmpty();

    void StringSearchWithPosOffEndOfString();

};
