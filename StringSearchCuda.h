#pragma once
#include <cstdint>

__global__ void CudaStringSearch(const char* original_string, const char* input, size_t* device_result);
